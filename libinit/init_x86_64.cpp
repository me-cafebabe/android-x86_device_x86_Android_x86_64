/*
 * Copyright (C) 2021 The LineageOS Project
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <android-base/file.h>

#include <libinit_dalvik_heap.h>
#include <libinit_utils.h>

#include "vendor_init.h"

#define DMI_PATH(entry) "/sys/class/dmi/id/" entry

void set_device_props() {
    std::string model, vendor;

    android::base::ReadFileToString(DMI_PATH("board_name"), &model);
    if (!model.empty())
        set_ro_build_prop("model", model, true);

    android::base::ReadFileToString(DMI_PATH("board_vendor"), &vendor);
    if (!vendor.empty())
        set_ro_build_prop("brand", vendor, true);
}

void vendor_load_properties() {
    set_dalvik_heap();
    set_device_props();
}
