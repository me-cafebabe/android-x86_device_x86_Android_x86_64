#
# Copyright (C) 2014 The Android-x86 Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common RevengeOS stuff.
$(call inherit-product, vendor/revengeos/config/common.mk)

# Inherit from x86_64 device
$(call inherit-product, device/x86/Android_x86_64/device.mk)

# Device identifier. This must come after all inclusions
PRODUCT_DEVICE := Android_x86_64
PRODUCT_NAME := revengeos_Android_x86_64
BOARD_VENDOR := Android-x86
PRODUCT_BRAND := Android-x86
PRODUCT_MODEL := Generic Android-x86_64
PRODUCT_MANUFACTURER := Android-x86
TARGET_VENDOR := Android-x86

# RevengeOS
TARGET_BOOT_ANIMATION_RES := 720
