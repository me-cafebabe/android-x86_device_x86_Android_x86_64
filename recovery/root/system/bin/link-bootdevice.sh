#!/system/bin/sh

# reference:
#    wait /dev/block/pci/pci0000:00/${ro.boot.bootdevice}
#    symlink /dev/block/pci/pci0000:00/${ro.boot.bootdevice} /dev/block/bootdevice

BOOTDEVICE="$(find /dev/block/ -name super | grep -v 'block/by-name' | tail -n 1 | cut -d '/' -f -6)"
ln -sf $BOOTDEVICE /dev/block/bootdevice
