#!/bin/bash -xe

[ -f "$OUT/ramdisk.img" ]
[ -f "$OUT/ramdisk-recovery.img" ]

MY_PATH=$(dirname $(realpath "$0"))

cd $OUT

mkdir -p iso/boot/grub
cp ramdisk.img iso/
cp ramdisk-recovery.img iso/
cp $MY_PATH/grub.cfg iso/boot/grub/
if [ -f "obj/KERNEL_OBJ/arch/x86/boot/bzImage" ]; then
cp obj/KERNEL_OBJ/arch/x86/boot/bzImage iso/
else
cp $MY_PATH/../prebuilt/bzImage iso/
fi

grub-mkrescue -o android-grub.iso iso
